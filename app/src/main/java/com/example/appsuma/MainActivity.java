package com.example.appsuma;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText cajaNum1;
    EditText cajaNum2;
    TextView resul;
    Button simSum;
    Button about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cajaNum1 = findViewById(R.id.num1);
        cajaNum2 = findViewById(R.id.num2);
        resul = findViewById(R.id.res);
        simSum = findViewById(R.id.sum);
        about = (Button) findViewById(R.id.about);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent about = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(about);
            }
        });
    }

    public void BotonSuma(View view){
        int nume1 = Integer.parseInt(cajaNum1.getText().toString());
        int nume2 = Integer.parseInt(cajaNum2.getText().toString());
        int resultado = nume1 + nume2;
        resul.setText(resultado);
        Toast.makeText(this, "Ya esta hecho", Toast.LENGTH_SHORT).show();
    }

}
